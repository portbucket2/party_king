using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JTween : MonoBehaviour
{
    public float target_float;
    public float start_float;
    public float end_float;
    public float speed;

    private void Awake()
    {
        enabled = false;
    }

    public void Initiate(float target_float, float start_float, float end_float, float speed)
    {
        this.target_float = target_float;
        this.start_float = start_float;
        this.end_float = end_float;
        this.speed = speed;
    }

    private void FixedUpdate()
    {
        target_float = Mathf.Lerp(target_float, end_float, Time.fixedDeltaTime * speed);
    }
}
