using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stereo : MonoBehaviour
{
    public int max_npc;
    public NPC npc_to_spawn;
    public GameObject spawner;
    public GameObject max;
    public float spawn_range;
    private bool max_reached = false;

    public Image increment_progress;
    public float increment_speed;

    public List<NPC> target_npcs;

    public Animator stereo_animator;
    private bool player_entered = false;
    private int is_playing = Animator.StringToHash("playing");

    public GameObject progress_bar;
    private float progress_interpolation = 1;

    public float duration;

    private Vector3 scale = Vector3.one;

    private void Awake()
    {
        scale = progress_bar.transform.localScale;
        increment_progress.fillAmount = 0;
    }

    private void FixedUpdate()
    {
        if (!player_entered)
        {
            if (progress_interpolation < 1)
            {
                progress_interpolation += Time.fixedDeltaTime * 1 / duration;
            }
            else
            {
                StopStereoDelayed();
            }

            scale.x = Mathf.Lerp(scale.y, 0, progress_interpolation);
            progress_bar.transform.localScale = scale;
        }

        if (progress_interpolation > 0)
        {
            increment_progress.fillAmount += Time.fixedDeltaTime * increment_speed;

            if (increment_progress.fillAmount >= 1)
            {
                increment_progress.fillAmount = 0;

                if (target_npcs.Count >= max_npc)
                {
                    max.SetActive(true);
                    max_reached = true;
                    increment_progress.gameObject.SetActive(false);

                    return;
                }

                SpawnNPC();
            }
        }
    }

    private void SpawnNPC()
    {
        NPC new_npc = Instantiate(npc_to_spawn);

        Vector3 offset = new Vector3(Random.Range(-spawn_range, spawn_range), 0, Random.Range(-spawn_range, spawn_range));

        new_npc.transform.position = spawner.transform.position + offset;

        new_npc.transform.rotation = Quaternion.Euler(Vector3.up * Random.Range(-30f, 30f)); ;

        target_npcs.Add(new_npc);

        new_npc.Dance();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            stereo_animator.SetBool(is_playing, true);
            player_entered = true;

            scale.x = scale.y;
            progress_bar.transform.localScale = scale;

            if (progress_interpolation >= 1 && !max_reached)
            {
                increment_progress.gameObject.SetActive(true);
                increment_progress.fillAmount = 0;
            }

            foreach (NPC npc in target_npcs)
            {
                npc.Dance();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            player_entered = false;
            progress_interpolation = 0;
            //Invoke("StopStereoDelayed", duration);
        }
    }

    private void StopStereoDelayed()
    {
        if (player_entered)
        {
            return;
        }
        stereo_animator.SetBool(is_playing, false);
        
        increment_progress.fillAmount = 0;
        increment_progress.gameObject.SetActive(false);

        foreach (NPC npc in target_npcs)
        {
            npc.Idle();
        }
    }
}
