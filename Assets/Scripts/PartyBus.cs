using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartyBus : MonoBehaviour
{
    public List<int> people_needed;
    public List<Text> people_needed_text;

    private Player player;

    public Animator vehicle_root_animator;

    public GameManager gameManager;

    private void Awake()
    {
        TextUpdate();
        gameManager = FindObjectOfType<GameManager>();
        player = FindObjectOfType<Player>();
    }

    private void TextUpdate()
    {
        for (int i = 0; i < people_needed.Count; i++)
        {
            people_needed_text[i].text = people_needed[i].ToString();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("npc"))
        {
            NPC npc = other.GetComponent<NPC>();

            if (people_needed[npc.id] <= 0)
            {
                people_needed[npc.id] = 0;
                other.gameObject.SetActive(false);
                player.RemoveFriend(npc.id);
                return;
            }

            people_needed[npc.id]--;
            TextUpdate();
            other.gameObject.SetActive(false);
            player.RemoveFriend(npc.id);
        }

        bool all_aboard = true;

        foreach (int item in people_needed)
        {
            if (item > 0)
            {
                all_aboard = false;
            }
        }

        if (all_aboard)
        {
            vehicle_root_animator.enabled = true;

            player.mesh_root.SetActive(false);
            gameManager.LevelEnd();

            foreach (NPC item in player.friend_list)
            {
                item.gameObject.SetActive(false);
            }

            player.GetComponent<CapsuleCollider>().enabled = false;
        }
    }
}
