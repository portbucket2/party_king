using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public List<ID_Map> id_map;

    public int next_level;

    private int activity_done = 0;

    public List<GameObject> activate_after_first_activity;

    public bool level_ended = false;

    public GameObject LevelEndUI;

    public void ActivityDone()
    {
        activity_done++;

        foreach (GameObject item in activate_after_first_activity)
        {
            item.SetActive(true);
        }
    }

    public void LevelEnd()
    {
        foreach (GameObject item in activate_after_first_activity)
        {
            item.SetActive(false);
        }
        level_ended = true;

        Invoke("ActivateLevelEndUI", 1.75f);
    }

    private void ActivateLevelEndUI()
    {
        LevelEndUI.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene(next_level);
    }
}

[System.Serializable]
public class ID_Map
{
    public Sprite icon;
    public Color font_color;
    public Color bg_color;
}
