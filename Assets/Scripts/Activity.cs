using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Activity : MonoBehaviour
{
    public Animator door_animator;

    public int max_to_spawn;
    public int spawned = 0;

    public NPC npc_to_spawn;
    public float spawn_after_seconds;
    public float spawn_range;
    public GameObject spawner;
    public GameObject run_target;

    public int friend_needed;
    public int friend_id_filter;
    public Text friend_needed_text;

    public SphereCollider col;

    public List<GameObject> hide_after_done;
    public List<Animator> animators_to_activate;
    public List<GameObject> activate_after_done;

    private Player player;

    private GameManager gameManager;

    private void Awake()
    {
        TextUpdate();
        player = FindObjectOfType<Player>();
        gameManager = FindObjectOfType<GameManager>();
    }

    private void TextUpdate()
    {
        friend_needed_text.text = friend_needed.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (friend_needed <= 0)
        {
            friend_needed = 0;
            return;
        }

        if (other.CompareTag("npc"))
        {
            NPC npc = other.GetComponent<NPC>();

            if (npc.id != friend_id_filter)
            {
                return;
            }

            if (npc.is_friend)
            {
                friend_needed--;
                TextUpdate();
                other.gameObject.SetActive(false);
                player.RemoveFriend(npc.id);
                player.friend_list.Remove(npc);

                if (friend_needed <= 0)
                {
                    foreach (GameObject item in hide_after_done)
                    {
                        item.SetActive(false);
                    }

                    foreach (GameObject item in activate_after_done)
                    {
                        item.SetActive(true);
                    }

                    foreach (Animator item in animators_to_activate)
                    {
                        item.enabled = true;
                    }

                    SpawnNPC();
                    gameManager.ActivityDone();
                }
            }
        }
    }

    private void SpawnNPC()
    {
        if (spawned < max_to_spawn)
        {
            NPC new_npc = Instantiate(npc_to_spawn);

            Vector3 offset = new Vector3(Random.Range(-spawn_range, spawn_range), 0, Random.Range(-spawn_range, spawn_range));

            new_npc.transform.position = spawner.transform.position;
            new_npc.spawner = this;
            new_npc.run_target = run_target.transform.position + offset;
            new_npc.run_towards_target = true;
            new_npc.Dance();
            new_npc.LookAtRandomAfterRun();

            spawned++;

            door_animator.Play("door_open", -1, 0);
        }

        Invoke("SpawnNPC", spawn_after_seconds);
    }
}
