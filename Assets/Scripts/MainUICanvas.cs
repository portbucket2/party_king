using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainUICanvas : MonoBehaviour
{
    public ResourceUI resource_ui;

    public ResourceUI AddNewResource(Sprite sprite, Color bg_color, Color font_color, float offset)
    {
        ResourceUI new_resource_ui = Instantiate(resource_ui);

        new_resource_ui.icon.sprite = sprite;
        new_resource_ui.bg.color = bg_color;
        new_resource_ui.count.color = font_color;

        new_resource_ui.transform.parent = transform;

        new_resource_ui.transform.localPosition = Vector3.zero;
        new_resource_ui.transform.localPosition += Vector3.down * offset;

        return new_resource_ui;
    }

}
