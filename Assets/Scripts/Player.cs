using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour
{
    public GameObject UI;

    private Vector3 intended_velocity = Vector3.zero;
    private Vector3 velocity = Vector3.zero;
    public float speed;
    private float intended_rotation = 180;
    public float angular_acceleration = 30;
    public float acceleration = 10;

    public Animator player_mesh_animator;

    private int idle_run_blend = Animator.StringToHash("idle_run");
    private int idle_dance_blend = Animator.StringToHash("idle_activity");

    public GameObject mesh_root;

    public GameObject joystick_base;
    public GameObject joystick_handle;

    private bool mouse_pressed = false;

    private Vector2 touch_diff = Vector2.zero;
    private float touch_diff_magnitude = 0;
    private Vector2 mousepos = Vector2.zero;
    private Vector2 touch_base = Vector2.zero;

    private float joy_max_distance = 142;
    public float joy_max_distance_multiplier = 0.049f;
    public float joy_come_back_speed = 1.5f;
    private float joy_angle = 0;

    private Rigidbody rb;

    private float idle_intended_blend_value = 0;
    private float idle_blend_value = 0;

    public int max_friend;
    public List<int> friend_count = new List<int>();
    public List<NPC> friend_list = new List<NPC>();
    //public List<TMP_Text> count_text;
    public MainUICanvas main_ui_canvas;
    public List<ResourceUI> resource_ui_list = new List<ResourceUI>();

    private int dance_trigger_count = 0;

    private GameManager gameManager;

    public Text debug_text;

    void Start()
    {
        joy_max_distance = Screen.height * joy_max_distance_multiplier;

        rb = GetComponent<Rigidbody>();
        gameManager = FindObjectOfType<GameManager>();

        for (int i = 0; i < gameManager.id_map.Count; i++)
        {
            ID_Map id_map = gameManager.id_map[i];

            ResourceUI new_resource_ui = main_ui_canvas.AddNewResource(id_map.icon, id_map.bg_color, id_map.font_color, i * 250f);

            resource_ui_list.Add(new_resource_ui);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.level_ended)
        {
            return;
        }

        mousepos = Input.mousePosition;

        if (Input.GetMouseButtonDown(0))
        {
            mouse_pressed = true;

            joystick_base.SetActive(true);

            touch_base = mousepos;
        }

        if (Input.GetMouseButtonUp(0))
        {
            mouse_pressed = false;

            joystick_base.SetActive(false);

            touch_diff = Vector2.zero;
            joystick_base.transform.position = Vector2.zero;

            intended_velocity = Vector3.zero;
        }

        if (Input.GetMouseButton(0))
        {
            touch_diff = mousepos - touch_base;
            touch_diff_magnitude = touch_diff.magnitude;

            if (touch_diff.magnitude > 1)
            {
                intended_velocity.x = touch_diff.x;
                intended_velocity.z = touch_diff.y;
                intended_rotation = Mathf.Atan2(intended_velocity.x, intended_velocity.z);
            }
        }

        if (touch_diff_magnitude < joy_max_distance)
        {
            joystick_handle.transform.position = mousepos;
        }
        else
        {
            joystick_handle.transform.position = touch_base + touch_diff.normalized * joy_max_distance;
        }

        if (touch_diff_magnitude > joy_max_distance * 0.75f)
        {
            touch_base = Vector2.Lerp(touch_base, mousepos, Time.deltaTime * joy_come_back_speed);
        }

        joystick_base.transform.position = touch_base;

        //debug_text.text = touch_diff_magnitude.ToString("F2");
    }

    private void FixedUpdate()
    {
        if (gameManager.level_ended)
        {
            UI.SetActive(false);
            rb.velocity = Vector3.zero;
            return;
        }

        mesh_root.transform.rotation = Quaternion.Euler(Vector3.up * intended_rotation * Mathf.Rad2Deg);

        velocity = Vector3.Lerp(velocity, intended_velocity.normalized * speed, Time.fixedDeltaTime * acceleration);

        player_mesh_animator.SetFloat(idle_run_blend, velocity.magnitude / speed);

        rb.velocity = velocity;

        idle_blend_value = Mathf.Lerp(idle_blend_value, idle_intended_blend_value, Time.fixedDeltaTime * 5f);

        player_mesh_animator.SetFloat(idle_dance_blend, idle_blend_value);
    }

    //private void OnTriggerEnter(Collision collision)
    //{

    //    if (collision.gameObject.CompareTag("Dance"))
    //    {
    //        idle_intended_blend_value = 1;
    //    }
    //}

    //private void OnCollisionExit(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("Dance"))
    //    {
    //        idle_intended_blend_value = 0;
    //    }
    //}

    public void AddFriend(int id, int count = 1)
    {
        friend_count[id] += count;
        CountTextUpdate(id);
    }

    public void RemoveFriend(int id, int count = 1)
    {
        friend_count[id] -= count;
        CountTextUpdate(id);
    }

    public bool MaxFriendReached()
    {
        int total = 0;

        foreach (int count in friend_count)
        {
            total += count;
        }

        return total >= max_friend;
    }

    private void CountTextUpdate(int id)
    {
        //count_text[id].text = friend_count[id].ToString();
        resource_ui_list[id].count.text = friend_count[id].ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Dance"))
        {
            dance_trigger_count++;

            idle_intended_blend_value = 1;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Dance"))
        {
            dance_trigger_count--;

            if (dance_trigger_count <= 0)
            {
                dance_trigger_count = 0;
                idle_intended_blend_value = 0;
            }
        }
    }


}
