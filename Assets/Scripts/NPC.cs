using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Props
{
    party_hat,
    beach_hat,
    surf_board,
    swimming_goggles
}

    public class NPC : MonoBehaviour
{
    public int id = 0;
    public List<GameObject> props;

    public Activity spawner;

    public Material neutral_mat;
    public Material friend_mat;
    public SkinnedMeshRenderer npc_mesh;

    public Animator npc_animator;

    private int idle_run_blend = Animator.StringToHash("idle_run");
    private int idle_dance_blend = Animator.StringToHash("idle_activity");

    private float idle_intended_blend_value = 0;
    private float idle_blend_value = 0;

    private JTween jTween;

    public bool is_friend = false;
    private Player player;
    private Vector3 rand_offset = Vector3.zero;
    public float rand_offset_range;
    private Vector3 velocity = Vector3.zero;
    private Vector3 intended_velocity = Vector3.zero;
    public float speed;
    public float run_towards_target_speed;
    public float acceleration;
    private Rigidbody rb;

    public Text text;

    public bool run_towards_target = false;
    public Vector3 run_target = Vector3.zero;

    private Vector3 after_run_direction = Vector3.zero;

    private int dance_trigger_count = 0;

    public SphereCollider col;

    private void Awake()
    {
        jTween = GetComponent<JTween>();
        rb = GetComponent<Rigidbody>();
        player = FindObjectOfType<Player>();
        col = GetComponent<SphereCollider>();
        RandomizeOffset();

        LookAtRandomAfterRun();
    }

    public void LookAtRandomAfterRun()
    {
        after_run_direction = Vector3.up * Random.Range(-360f, 360f);
    }

    public void Dance()
    {
        //npc_animator.SetFloat(idle_dance_blend, 1);
        idle_intended_blend_value = 1;
    }

    public void Idle()
    {
        //npc_animator.SetFloat(idle_dance_blend, 0);
        idle_intended_blend_value = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (is_friend)
            {
                return;
            }

            if (player.MaxFriendReached())
            {
                return;
            }

            run_towards_target = false;
            is_friend = true;
            npc_mesh.material = friend_mat;
            player.AddFriend(id);
            player.friend_list.Add(this);
            if (spawner != null)
            {
                spawner.spawned--;
            }
        }

        if (other.gameObject.CompareTag("Dance"))
        {
            dance_trigger_count++;
            Dance();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Dance"))
        {
            dance_trigger_count--;

            if (dance_trigger_count <= 0)
            {
                dance_trigger_count = 0;

                if (id == 0)
                {
                    Idle();
                }
            }
        }
    }

    private void RandomizeOffset()
    {
        rand_offset = new Vector3(Random.Range(-rand_offset_range, rand_offset_range), 0, Random.Range(-rand_offset_range, rand_offset_range));
    }

    bool randomize_done = false;

    private void FixedUpdate()
    {
        if (run_towards_target)
        {
            intended_velocity = (run_target - transform.position);

            if (intended_velocity.sqrMagnitude > 0.05f)
            {
                velocity = Vector3.Lerp(velocity, intended_velocity * run_towards_target_speed, Time.fixedDeltaTime * acceleration);
                transform.LookAt(transform.position + intended_velocity, Vector3.up);
            }
            else
            {
                velocity = Vector3.Lerp(velocity, Vector3.zero, Time.fixedDeltaTime * acceleration);

                if (velocity.sqrMagnitude <= 0.02)
                {
                    run_towards_target = false;
                    velocity = Vector3.zero;
                }

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(after_run_direction), Time.fixedDeltaTime * 5f);
            }

            rb.velocity = velocity;

        }
        else if (is_friend)
        {
            intended_velocity = ((player.transform.position + rand_offset) - transform.position);

            if (intended_velocity.sqrMagnitude > 1f)
            {
                velocity = Vector3.Lerp(velocity, intended_velocity * speed, Time.fixedDeltaTime * acceleration);
                randomize_done = false;
            }
            else
            {
                velocity = Vector3.Lerp(velocity, Vector3.zero, Time.fixedDeltaTime * acceleration);

                if (!randomize_done)
                {
                    randomize_done = true;
                    RandomizeOffset();
                }
            }

            rb.velocity = velocity;
            transform.LookAt(transform.position + intended_velocity, Vector3.up);
        }

        npc_animator.SetFloat(idle_run_blend, velocity.magnitude / speed);

        idle_blend_value = Mathf.Lerp(idle_blend_value, idle_intended_blend_value, Time.fixedDeltaTime * 5f);

        npc_animator.SetFloat(idle_dance_blend, idle_blend_value);

        //text.text = (player.transform.position - transform.position).sqrMagnitude.ToString();
    }

    public void ConvertNpc(int new_id)
    {
        id = new_id;

        npc_mesh.material = neutral_mat;
    }

    public void EnableProps(List<Props> prop_ids)
    {
        foreach (Props id in prop_ids)
        {
            props[(int)id].SetActive(true);
        }
    }

    public void EnableCol(bool value)
    {
        col.enabled = value;
    }
}
