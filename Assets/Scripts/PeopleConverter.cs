using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PeopleConverter : MonoBehaviour
{
    public Image progress;

    public int convert_from;
    public int convert_to;
    public List<Props> enable_props;
    public Material output_friend_material;

    public Animator door_animator;

    public float spawn_after_seconds;
    public float spawn_range;
    public GameObject spawner;
    public GameObject run_target;
    public GameObject run_inside_target;

    public int friend_max;
    public int friend_have;
    public List<NPC> friends_list;
    public Text friend_needed_text;

    public float npc_hide_delay;

    //public List<GameObject> hide_after_done;
    //public List<Animator> animators_to_activate;
    //public List<GameObject> activate_after_done;

    private Player player;

    private GameManager gameManager;

    private bool automation_started = false;

    private void Awake()
    {
        TextUpdate();
        player = FindObjectOfType<Player>();
        gameManager = FindObjectOfType<GameManager>();
    }

    private void TextUpdate()
    {
        friend_needed_text.text = friend_have.ToString() + "/" + friend_max.ToString();
    }

    List<NPC> npc_to_hide = new List<NPC>();
    private void HideNPC()
    {
        npc_to_hide[0].gameObject.SetActive(false);
        npc_to_hide[0].run_towards_target = false;
        npc_to_hide.RemoveAt(0);
    }


    private void Update()
    {
        if (progress.gameObject.activeSelf)
        {
            progress.fillAmount += Time.deltaTime / spawn_after_seconds;

            if (progress.fillAmount >= 1)
            {
                progress.gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (friend_have >= friend_max)
        {
            friend_have = friend_max;
            return;
        }

        if (other.CompareTag("npc"))
        {
            NPC npc = other.GetComponent<NPC>();

            if (npc.id != convert_from)
            {
                return;
            }

            if (npc.is_friend)
            {
                friend_have++;
                TextUpdate();
                //other.gameObject.SetActive(false);
                npc.is_friend = false;
                npc.run_towards_target = true;
                npc.run_target = run_inside_target.transform.position;
                npc_to_hide.Add(npc);
                npc.EnableCol(false);
                Invoke("HideNPC", npc_hide_delay);
                player.RemoveFriend(npc.id);
                player.friend_list.Remove(npc);
                friends_list.Add(npc);



                //if (friend_max <= 0)
                //{
                //    foreach (GameObject item in hide_after_done)
                //    {
                //        item.SetActive(false);
                //    }

                //    foreach (GameObject item in activate_after_done)
                //    {
                //        item.SetActive(true);
                //    }

                //    foreach (Animator item in animators_to_activate)
                //    {
                //        item.enabled = true;
                //    }

                //    SpawnNPC();
                //    gameManager.ActivityDone();
                //}

                if (!automation_started)
                {
                    automation_started = true;
                    
                    progress.gameObject.SetActive(true);
                    progress.fillAmount = 0;
                    
                    Invoke("SpawnNPC", spawn_after_seconds);
                }
            }
        }
    }

    private void SpawnNPC()
    {
        if (friends_list.Count > 0)
        {
            NPC converted_npc = friends_list[0];

            Vector3 offset = new Vector3(Random.Range(-spawn_range, spawn_range), 0, Random.Range(-spawn_range, spawn_range));

            converted_npc.gameObject.SetActive(true);
            converted_npc.transform.position = spawner.transform.position;
            converted_npc.is_friend = false;
            converted_npc.spawner = null;
            converted_npc.run_target = run_target.transform.position + offset;
            converted_npc.run_towards_target = true;
            converted_npc.Dance();
            converted_npc.ConvertNpc(convert_to);
            converted_npc.EnableProps(enable_props);
            converted_npc.friend_mat = output_friend_material;
            converted_npc.EnableCol(true);
            converted_npc.LookAtRandomAfterRun();

            door_animator.Play("door_open", -1, 0);

            friends_list.RemoveAt(0);
            friend_have--;
            TextUpdate();
            
            if (friends_list.Count > 0)
            {
                progress.gameObject.SetActive(true);
                progress.fillAmount = 0;

                Invoke("SpawnNPC", spawn_after_seconds);
            }
            else
            {
                automation_started = false;
            }
        }
    }
}
