using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    private Transform cam_transform;
    void Start()
    {
        cam_transform = Camera.main.transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.rotation = Quaternion.LookRotation(transform.position - cam_transform.position);
    }
}
